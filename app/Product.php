<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Product
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $id_type
 * @property string|null $description
 * @property float|null $unit_price
 * @property float|null $promotion_price
 * @property string|null $image
 * @property string|null $unit
 * @property int|null $new
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereIdType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereNew($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product wherePromotionPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereUnit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereUnitPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Product whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Product extends Model
{
    //
    public $table = 'products';
    public static $STATUS = [
        'waiting' => 0,
        'accept' => 1,
        'shipping' => 2,
        'success' => 3
    ];

    protected $fillable = ['name', 'id_type', 'unit_price', 'promotion_price', 'image', 'unit', 'new', 'amount', 'price_import'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments(){
        return $this->hasMany(Comment::class);
    }

    public function orderDetails () {
        return $this->hasMany(OrderDetail::class, 'id_product', 'id');
    }

    public function productDetail () {
        return $this->hasOne(ProductDetail::class);
    }

    public function exchanges() {
        return $this->hasMany(Exchange::class, 'id_product_exchange', 'id');
    }

    public function refund(){
        return $this->hasMany(Exchange::class, 'id_product_refund', 'id');
    }
}
