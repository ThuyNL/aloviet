<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Order
 *
 * @property int $id
 * @property int|null $id_customer
 * @property string|null $date_order
 * @property float|null $total tổng tiền
 * @property string|null $payment hình thức thanh toán
 * @property string|null $note
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereDateOrder($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereIdCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order wherePayment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Order whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Order extends Model
{
    //
    public $table = 'orders';

    protected $fillable = ['id_customer', 'name', 'phone', 'address', 'date_order', 'total', 'payment', 'status', 'note', 'received_date'];

    public function orderDetails() {
        return $this->hasMany(OrderDetail::class, 'id_bill', 'id');
    }
    public function order_details() {
        return $this->hasMany(OrderDetail::class, 'id_bill', 'id')->where('quantity', '>', 0);
    }
    public function user() {
        return $this->belongsTo(User::class, 'id_customer');
    }

    public function exchanges() {
        return $this->hasMany(Exchange::class, 'id_order', 'id');
    }

    public function bill() {
        return $this->hasOne(Bill::class, 'order_id', 'id');
    }
}
