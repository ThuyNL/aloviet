<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    public $table = 'product_details';

    protected $fillable = ['screen', 'system', 'camera_after', 'camera_before', 'cpu', 'ram', 'cache_in', 'cache_tag', 'sim_tag', 'description', 'product_id'];

    public function product() {
        return $this->belongsTo(Product::class);
    }
}
