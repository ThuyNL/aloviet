<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\OrderDetail
 *
 * @property int $id
 * @property int $id_bill
 * @property int $id_product
 * @property int $quantity số lượng
 * @property float $unit_price
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderDetail newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderDetail query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderDetail whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderDetail whereIdBill($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderDetail whereIdProduct($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderDetail whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderDetail whereUnitPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\OrderDetail whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrderDetail extends Model
{
    //
    public $table = 'order_detail';

    protected $fillable = ['id_bill','id_product', 'quantity', 'import_price', 'unit_price', 'status'];

    public function order() {
        return $this->belongsTo(Order::class, 'id_bill');
    }

    public function product() {
        return $this->belongsTo(Product::class, 'id_product');
    }

    public function exchanges() {
        return $this->hasMany(Exchange::class, 'id_order_detail', 'id');
    }
}
