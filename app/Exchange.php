<?php

namespace App;

use const http\Client\Curl\PROXY_HTTP;
use Illuminate\Database\Eloquent\Model;

class Exchange extends Model
{
    public $table = 'exchange';
    protected $fillable = ['id_order', 'id_order_detail', 'id_product_refund', 'unit_price_refund', 'id_product_exchange','unit_price_exchange', 'amount','payment_amount', 'received_date', 'status'];

    public function order() {
        return $this->belongsTo(Order::class, 'id_order');
    }

    public function orderDetail () {
        return $this->belongsTo(OrderDetail::class, 'id_order_detail');
    }

    public function productExchange() {
        return $this->belongsTo(Product::class, 'id_product_exchange');
    }

    public function productRefund() {
        return $this->belongsTo(Product::class, 'id_product_refund');
    }
}
