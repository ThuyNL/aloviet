<?php

namespace App\Http\Controllers;

use App\Jobs\SendMailOrderToUser;
use App\Order;
use App\OrderDetail;
use App\Product;
use App\Mail\OrderSuccess;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Cart;
use Mockery\Exception;

class OrderController extends Controller
{
    //
    public function ship(Request $request, $orderId)
    {
        $order = Order::find($orderId);

        // Ship order...

        $user = $request->user();
        if ($user) {
            Mail::to($user->email)->send(new OrderSuccess($order));
        }
    }

    public function getOrder(){
        $user = Auth::user();
        $cart = Cart::content();
        $total = Cart::subtotal();
        $image = [];
        foreach ($cart as $row){
            $image[$row->id] = Product::find($row->id)->image;
        }
        return view('site.order.order',compact(['user','cart','image','total']));
    }
    public function postOrder(Request $request){
        $message = '';
        $product_over = [];
        foreach (Cart::content() as $item) {
            $product = Product::find($item->id);
            if ($item->qty > ($product->amount - $product->amount_ordered)) {
                array_push($product_over, $product->name);
            }
        }
        if (count($product_over) > 0){
            $message = 'Chúng tôi không có đủ số lượng sản phẩm';
            foreach ($product_over as $value) {
                $message = $message.' '.$value. ',';
            }
            $message = $message. ' để đáp ứng đơn hàng của bạn';
           return redirect('dat-hang')->with('thongbao',$message);
        }
        DB::beginTransaction();
        try{
            $tran = new Order();
            $tran->id_customer = Auth::id();
            $tran->name = Auth::user()->full_name;
            $tran->phone = Auth::user()->phone;
            $tran->address = Auth::user()->address;
            $tran->date_order = date('Y-m-d');
            $tran->total = floatval(str_replace(',','',Cart::subtotal()));
            $tran->payment = $request->payment_method;
            $tran->note = $request->note;
            $tran->save();
            foreach (Cart::content() as $row){
                $product = Product::find($row->id);
                $order = new OrderDetail();
                $order->id_bill = $tran->id;
                $order->id_product = $row->id;
                $order->quantity = $row->qty;
                $order->unit_price = $row->price;
                $order->import_price = $product->price_import;
                $order->save();
                $product->amount_ordered = $product->amount_ordered + $row->qty;
                $product->save();
            }
            DB::commit();
        } catch(\Exception $e) {
            DB::rollBack();
        }
        Cart::destroy();
//        $email = $request->user()->email;
//        $order_id = $tran->id;
        //SendMailOrderToUser::dispatch($email, $order_id);
        //$this->ship($request, $tran->id);
        $message = 'Cảm ơn bạn đã đặt hàng sản phẩm của chúng tôi!';
        return redirect('/')->with('thongbao',$message);
    }
}
