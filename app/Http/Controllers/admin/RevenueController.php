<?php

namespace App\Http\Controllers\admin;

use App\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RevenueController extends Controller
{
    public function index () {
        $total = Order::query()
            ->selectRaw('sum(orders.total) as total')
            ->where('status', 3)
            ->get();
        $revenue = $total[0]->total;
        return view('admin.revenue.index', [
            'revenue' => $revenue
        ]);
    }

    public function search (Request $request) {
        $date_from = null;
        $date_to = null;
        $query = Order::query()
            ->selectRaw('sum(orders.total) as total')
            ->where('status', 3);
        if ($request->date_from) {
            $query->where('received_date', '>=', $request->date_from);
            $date_from = $request->date_from;
        }
        if ($request->date_to) {
            $query->where('received_date', '>=', $request->date_to);
            $date_to = $request->date_to;
        };
            $total = $query->get();
        $revenue = $total[0]->total;
        return view('admin.revenue.index', [
            'revenue' => $revenue,
            'date_from' => $date_from,
            'date_to' => $date_to
        ]);
    }
}
