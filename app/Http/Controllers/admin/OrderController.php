<?php

namespace App\Http\Controllers\admin;

use App\Order;
use App\OrderDetail;
use App\Product;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{

    public function view(){
        $orders = Order::query()
            ->where('status','!=', 5)
            ->orderBy('created_at', 'DESC')
            ->paginate(15);
        return view('admin.order.view',[
            'orders' => $orders,
            'status' => null
        ]);
    }

    // tạo đơn hàng mới
    public function createOrder() {
        return view('admin.order.create');
    }

    public function postOrder(Request $request) {
        $customer = User::find($request->id_customer);
        if (!$customer) {
            return redirect('admin/order/create')->with('thongbao', 'Không thể thực hiện vì không tồn tại khách hàng mã số '.$request->id_product);
        }
        $product = Product::find($request->id_product);
        if (!$product){
            return redirect('admin/order/create')->with('thongbao', 'Không thể thực hiện vì không tồn tại mã sản phẩm '.$request->id_product);
        }
        $total = $product->unit_price * $request->quantity;
        $new_order = new Order();
        $new_order->id_customer = $request->id_customer;
        $new_order->date_order = date('Y-m-i');
        $new_order->total = $total;
        $new_order->payment = $request->payment;
        if ($request->note) {
            $new_order->note = $request->note;
        }
        $new_order->status = $request->status;
        $new_order->save();

        $newDetail = new OrderDetail();
        $newDetail->id_bill = $new_order->id;
        $newDetail->id_product = $request->id_product;
        $newDetail->quantity = $request->quantity;
        $newDetail->unit_price = $product->unit_price;
        $newDetail->save();
        return redirect('admin/transaction/changeStatus/'.$new_order->id)->with('thongbao', 'Tạo đơn hàng mới thành công!');

    }
    //xóa một đơn hàng
    public function delete($id){
        $order = Order::find($id);
        if(!$order) return view('admin.product.error');
        $order->load('orderDetails');
        foreach ($order->orderDetails as $item) {
            $item->delete();
        }
        $order->delete();
        return redirect('admin/order/view')->with('thongbao','Xóa đơn hàng thành công');
    }
    //xóa nhiều đơn hàng
    public function deleteMultiple(Request $request){
        foreach ($request->allVals as $row){
            $order = OrderDetail::find($row);
            if(!$order) return response()->json('fail');
            $tran = Order::find($order->id_bill);
            if($tran->total - $order->unit_price*$order->quantity ==0 ){
                $tran->delete();
            }
            else{
                $tran->total = $tran->total - $order->unit_price*$order->quantity;
                $tran->save();
            }
            $order->delete();
        }



        return response()->json('ok');
//        $order = OrderDetail::find($id);
//        if(!$order) return view('admin.product.error');
//        $tran = Order::find($order->id_bill);
//        if($tran->total - $order->unit_price*$order->quantity ==0 ){
//            $tran->delete();
//        }
//        else{
//            $tran->total = $tran->total - $order->unit_price*$order->quantity;
//            $tran->save();
//        }
//        $order->delete();
//        return redirect('admin/order/view')->with('thongbao','Xóa đơn hàng thành công');
    }
    //tìm kiếm order
    public function search(Request $request){
        $date_from = null;
        $date_to = null;
        $order_id = null;
        $status = null;
        $input = $request->all();
        $query = Order::query()
            ->where('status', '!=', 5);
        if ($input['date_from']) {
            $query->where('created_at', '>=', date('Y-m-d',strtotime($input['date_from'])));
            $date_from = $input['date_from'];
        }
        if ($input['date_to']) {
            $query->where('created_at', '<=', date('Y-m-d',strtotime($input['date_to'])));
            $date_to = $input['date_to'];
        }
        if (isset($input['status']) && $input['status'] != '') {
            $status = (int)$input['status'];
            $query->where('status', $status);
        }
        if ($input['order_id'] && (int)$input['order_id']) {
            $query->where('id', (int)$input['order_id']);
            $order_id = $input['order_id'];
        }
        if (!$input['date_from'] && !$input['date_to'] && !$input['order_id'] && !$input['status']) {
            return redirect('admin/order/view');
        }
            $orders = $query->orderBy('created_at', 'DESC')
            ->get();
        return view('admin.order.view',[
            'orders' => $orders,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'order_id' => $order_id,
            'status' => $status
        ]);
    }
}
