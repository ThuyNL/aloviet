<?php

namespace App\Http\Controllers\admin;

use App\Exchange;
use App\OrderDetail;
use App\Product;
use App\ProductDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    //hiển thị danh sách sản phẩm
    public function view(Request $request){
        $product = Product::where('id','<>',0);
        $total = count($product->get());
        $product = $product->paginate(5);
//        if(isset($request->page)&&($request->page<1||$request->page>ceil($total/5)))
//            return view('admin.product.error');
        return view('admin.product.view',[
            'product' => $product,
            'total' => $total
        ]);
    }

    // lấy thông tin sản phẩm theo danh mục
    public function getProductByCategory($id){
      $productsByCate = Product::where('id_type', $id)->get();
      $productsByCate = $productsByCate->toArray();
      return response()->json($productsByCate);
    }

    //lấy số lượng sản phẩm của đơn hàng
    public function getQuantity($id) {
        $inputs = explode('_', $id);
        $id_order_detail = (int)$inputs[0];
        $quantity = 0;
        $order_detail = OrderDetail::find($id_order_detail);
        $exchange_orders = Exchange::where('id_order_detail', $id_order_detail)->get();
        if (count($exchange_orders) > 0) {
            $count_amount_exchange = 0;
            foreach ($exchange_orders as $exchange){
                $count_amount_exchange += $exchange->amount;
            }
            $sub = $order_detail->quantity - $count_amount_exchange;
            $sub > 0 ? $quantity = $sub : $quantity = 0;
        } else {
            $quantity = $order_detail->quantity;
        }
        return response()->json($quantity);
    }
    //hiển thị form thông tin sản phẩm
    public function add(){
        return view('admin.product.add');
    }
    //xử lý thêm thông tin sản phẩm
    public function postAdd(Request $request){
        $data = $request->all();
        $product = new Product();
        $product->name = $data['name'];
        $product->unit_price =floatval(str_replace(',','',$data['price']));
        if ($data['discount']) {
            $product->promotion_price = floatval(str_replace(',','',$data['discount']));
        }
        $product->new = $data['new'];
        $product->id_type = $request->cat;
        $file = $request->file('image');
        $duoi = $file->getClientOriginalExtension();
        if ($duoi != 'jpg' && $duoi != 'png') {
            return redirect('admin/product/add')->with('thongbao', 'Bạn phải chọn file ảnh')->withInput();
        }

        do {
            $name = str_random(4).'_'.$file->getClientOriginalName();
        }while ((file_exists("source/image/product/.$name")) );

        $product->image = $name;
        $file->move('source/image/product/', $name);
        $product->amount = $data['amount'];
        $product->price_import = floatval(str_replace(',','',$data['price_import']));
        $product->save();

        $params = ['screen', 'system', 'camera_after', 'camera_before', 'cpu', 'ram', 'cache_in', 'cache_tag', 'sim_tag', 'description'];
        $input = [];
        foreach ($params as $item) {
            if ($data[$item]) {
                $input[$item] = $data[$item];
            }
        }
        if (count($input) > 0) {
            $input['product_id'] = $product->id;
            ProductDetail::create($input);
        }
        return redirect('admin/product/add')->with('thongbao', 'Thêm sản phẩm thành công');
    }
    //hiển thị form sửa thông tin sản phẩm
    public function edit($id){
        $product = Product::find($id);
        $product = $product->load('productDetail');
        if(!$product) return view('admin.product.error');
        return view('admin.product.edit',compact('product'));
    }
    //xử lý sửa thông tin sản phẩm
    public function postEdit($id,Request $request){
        $data = $request->all();
        $product = Product::find($id);
        $product->name = $data['name'];
        $product->unit_price =floatval(str_replace(',','',$data['price']));
        $product->promotion_price = floatval(str_replace(',','',$data['discount']));
        $product->new = $data['new'];
        $product->id_type = $data['cat'];
        if($request->hasFile('image')){
            $file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png') {
                return redirect('admin/product/edit/'.$id)->with('loi', 'Bạn phải chọn file ảnh')->withInput();
            }
            $tencu = $file->getClientOriginalName();
            do {
                $name = str_random(4).$file->getClientOriginalName();
            }while ((file_exists("source/image/product/.$name")) );

            $product->image = $name;
            $file->move('source/image/product/',$name);
            if(file_exists("source/image/product/".$tencu))
                unlink('source/image/product/'.$tencu);
        }
        $product->amount = $data['amount'];
        $product->price_import = floatval(str_replace(',','',$data['price_import']));
        $product->save();

        $params = ['screen', 'system', 'camera_after', 'camera_before', 'cpu', 'ram', 'cache_in', 'cache_tag', 'sim_tag', 'description'];
        $input = [];
        foreach ($params as $item) {
            if ($data[$item]) {
                $input[$item] = $data[$item];
            }
        }
        if (count($input) > 0) {
            $detail = ProductDetail::where('product_id', $product->id)->first();
            $detail->update($input);
        }
        return redirect('admin/product/view')->with('thongbao', 'Chỉnh sửa sản phẩm thành công');
    }
    //xóa sản phẩm
    public function delete($id){
        $product = Product::find($id);
        if(!$product) return view('admin.product.error');
        $product->delete();
        return redirect('admin/product/view')->with('thongbao','Xóa sản phẩm thành công');
    }
    //xóa nhiều sản phẩm
    public function deleteMultiple(Request $request){
        foreach ($request->allVals as $row){
            $product = Product::find($row);
            if(!$product) return response()->json('fail');
            $product->delete();
        }
        return response()->json('ok');
    }
    //tìm kiếm sản phẩm
    public function search(Request $request){
        if($request->name==''){
            $product = Product::where('id_type',$request->catalog);
            $total = count($product->get());
            $product = $product->paginate(5);
        }
        else{
            if($request->catalog==''){
                $product = Product::where('name','like',$request->name.'%');
                $total = count($product->get());
                $product = $product->paginate(5);
            }else{
                $product = Product::where('name','like',$request->name.'%')
                    ->where('id_type',$request->catalog);
                $total = count($product->get());
                $product = $product->paginate(5);
            }
        }
        if(!$product) return redirect('admin/product/error');
        $product->setPath('./admin/product/search?name='.$request->name.'&catalog='.$request->catalog);
        return view('admin.product.view',compact(['product','total']));

    }
}
