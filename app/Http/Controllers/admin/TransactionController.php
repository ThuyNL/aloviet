<?php

namespace App\Http\Controllers\admin;

use App\Bill;
use App\Catalog;
use App\Exchange;
use App\Order;
use App\OrderDetail;
use App\Product;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Mockery\Exception;

class TransactionController extends Controller
{
    //
    public function view(){
        $trans = Order::query()
            ->with('bill')
            ->where('status', 5)
            ->orderBy('created_at', 'DESC')
            ->paginate(15);
        return view('admin.transaction.view',[
            'tran' => $trans
        ]);
    }
    //
    public function chitiet($id){
        $order = Order::find($id);
        $order->load('orderDetails', 'orderDetails.product');
        $order = $order->toArray();
        return response()->json($order);
    }
    ////chỉnh sửa status
    public function changeStatus($id){
        $is_success = false;
        $exchangable = false;
        $is_updated = true;
        $refundable = false;
        $remain_time = null;
        $order = Order::find($id);
        $order = $order->load('order_details', 'order_details.product', 'exchanges', 'exchanges.productExchange', 'exchanges.productRefund');
        if ($order->received_date) {
            $now = Carbon::now();
            $received_date = new Carbon($order->received_date);
            $received_date = $received_date->addDays(8);
            if ($received_date > $now) {
                $remain_time = $received_date->diffInDays($now);
            } else {
                $remain_time = 0;
            }
        }

        if($order->status == 3 || $order->status == 4) {
            $quantity_order = 0;
            $count_exchange_success = 0;
            foreach ($order->orderDetails as $detail) {
                $quantity_order += $detail->quantity;
            }

            foreach ($order->exchanges as $exchange){
                $count_exchange_success += ($exchange->status == 3) ? 1: 0;
            }
            if ($quantity_order > 0 && $remain_time > 0){
                $exchangable = true;
            }
            if(($count_exchange_success == count($order->exchanges))){
                $is_updated = false;
                if ($exchangable == false)
                $is_success = true;
            }
        }

        return view('admin.transaction.edit', [
            'order' => $order,
            'remain_time' => $remain_time,
            'is_success' => $is_success,
            'exchangable'=> $exchangable,
            'is_updated' => $is_updated,
            'refundable' => $refundable
        ]);
    }

    public function postChangeStatus($id, Request $request) {
        $input = $request->all();
        $order = Order::find($id);
        $status_order = $request->status_order;
        if ($status_order) {
            if ($status_order == 1) {
                $order = $order->load('orderDetails', 'orderDetails.product');
                foreach ($order->orderDetails as $detail) {
                    $product = Product::find($detail->product->id);
                    if ($product) {
                        $product->amount = $product->amount - $detail->quantity;
                        $product->amount_ordered = $product->amount_ordered - $detail->quantity;
                        $product->save();
                    }
                }
            }
            if ($status_order == 3) {
                $order->received_date = date('Y-m-d H:i:s');
            }

            $order->status = $status_order;
            $order->save();
            return redirect('admin/transaction/changeStatus/'.$id)->with('thongbao', 'Cập nhật trạng thái thành công');
        } else {
            if ($order->status == 4) {
                $exchange_orders = Exchange::where('id_order',$id)->get();
                foreach ($exchange_orders as $exchange){
                    if (array_key_exists('status_exchange_'.$exchange->id, $input)) {
                        $status_exchange = $input['status_exchange_'.$exchange->id];
                        if ($status_exchange == 1) {
                            $product_exchange = Product::find($exchange->id_product_exchange);
                            $product_exchange->amount_ordered = $product_exchange->amount_ordered - $exchange->amount;
                            $product_exchange->amount = $product_exchange->amount - $exchange->amount;
                            $product_exchange->save();
                        }
                        if ($status_exchange == 3) {
                            DB::beginTransaction();
                            try {
                                $order_detail = OrderDetail::find($exchange->id_order_detail);
                                if($order_detail->quantity == $exchange->amount) {
                                    $order_detail->delete();
                                } else {
                                    $order_detail->quantity = $order_detail->quantity - $exchange->amount;
                                    $order_detail->save();
                                }
                                $new_detail = new OrderDetail();
                                $new_detail->id_bill = $id;
                                $new_detail->id_product = $exchange->id_product_exchange;
                                $new_detail->quantity = $exchange->amount;
                                $new_detail->unit_price = $exchange->unit_price_exchange;
                                $new_detail->status = 0;
                                $new_detail->save();
                                $order->total = $order->total + $exchange->payment_amount;
                                $order->save();
                                DB::commit();
                            } catch (\Exception $e) {
                                DB::rollBack();
                            }
                            $exchange->received_date = date('Y-m-d H:i:s');
                        }
                        $exchange->status = $status_exchange;
                        $exchange->save();
                    }
                }
                return redirect('admin/transaction/changeStatus/'.$id)->with('thongbao', 'Cập nhật trạng thái thành công');
            }
        }
    }

    // xử lý xuất hóa đơn
    public function exportBill($id) {
        $order = Order::find($id);
        $order = $order->load('orderDetails');
        DB::beginTransaction();
        try{
            $order->status = 5;
            $order->save();
            foreach ($order->orderDetails as $detail) {
                $detail->status = 1;
                $detail->save();
            }
            $bill = Bill::create([
                'order_id' => $id,
                'user_id' => $order->id_customer,
                'total' => $order->total,
                'received_date' => $order->received_date
            ]);
            DB::commit();
        }catch (\Exception $e) {
            DB::rollBack();
        }
        return redirect('admin/transaction/view')->with('thongbao', 'Xuất hóa đơn cho đơn hàng #'.$id. ' thành công');
    }

    // Hiển thị hóa đơn
    public function getBill ($id){
        $order = Order::with(['bill' => function ($query) use ($id) {
            $query->where('order_id', $id);
        }])
            ->where('status', 5)
            ->first();
        $order = $order->load('orderDetails', 'orderDetails.product');
        return view('admin.transaction.bill', [
            'order' => $order
        ]);
    }

    //xử lý thêm thông tin sản phẩm
    public function postAdd(Request $request){
        $tran = new Order();
        $tran->name = $request->name;
        $tran->unit_price =floatval(str_replace(',','',$request->price));
        $tran->promotion_price = floatval(str_replace(',','',$request->discount));
        $tran->new = $request->new;
        $tran->description = $request->mota;
        $tran->id_type = $request->cat;
        $file = $request->file('image');
        $duoi = $file->getClientOriginalExtension();
        if ($duoi != 'jpg' && $duoi != 'png') {
            return redirect('admin/order/add')->with('thongbao', 'Bạn phải chọn file ảnh')->withInput();
        }

        do {
            $name = str_random(4).$file->getClientOriginalName();
        }while ((file_exists("source/image/Order/.$name")) );

        $tran->image = $name;
        $file->move('source/image/Order/', $name);
        $tran->save();
        return redirect('admin/Order/add')->with('thongbao', 'Thêm sản phẩm thành công');
    }
    //hiển thị form sửa thông tin sản phẩm
    public function edit($id){
        $tran = Order::find($id);
        if(!$tran) return view('admin.Order.error');
        return view('admin.Order.edit',compact('Order'));
    }
    //xử lý sửa thông tin sản phẩm
    public function postEdit($id,Request $request){
        $tran = Order::find($id);
        $tran->name = $request->name;
        $tran->unit_price =floatval(str_replace(',','',$request->price));
        $tran->promotion_price = floatval(str_replace(',','',$request->discount));
        $tran->new = $request->new;
        $tran->description = $request->mota;
        $tran->id_type = $request->cat;
        if($request->hasFile('image')){
            $file = $request->file('image');
            $duoi = $file->getClientOriginalExtension();
            if ($duoi != 'jpg' && $duoi != 'png') {
                return redirect('admin/Order/edit/'.$id)->with('loi', 'Bạn phải chọn file ảnh')->withInput();
            }
            $tencu = $file->getClientOriginalName();
            do {
                $name = str_random(4).$file->getClientOriginalName();
            }while ((file_exists("source/image/Order/.$name")) );

            $tran->image = $name;
            $file->move('source/image/Order/',$name);
            if(file_exists("source/image/Order/".$tencu))
                unlink('source/image/Order/'.$tencu);
        }
        $tran->save();
        return redirect('admin/Order/edit/'.$id)->with('thongbao', 'Chỉnh sửa sản phẩm thành công');
    }

    // Xử lý hoàn đơn
    public function refundOrder($id) {
        $order = Order::find($id);
        $order->load('orderDetails');
        foreach ($order->orderDetails as $item) {
            $product = Product::find($item->id_product);
            if($product){
                DB::beginTransaction();
                try {
                    if($order->status == 0){
                        $product->amount_ordered -= $item->quantity;
                        $product->save();
                    } else {
                        $product->amount += $item->quantity;
                        $product->save();
                    }
                    $orderDetail = OrderDetail::find($item->id);
                    $orderDetail->delete();
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollBack();
                }
            } else {
                return back()->with('thongbao', 'Không thể thực hiện vì không tồn tại mã sản phẩm '.$product->id);
            }
        }
        $order->delete();
        return redirect('admin/order/view')->with('thongbao', 'Bạn đã hoàn đơn cho đơn hàng mã '.$id. ' thành công!');
    }

    // xử lý hoàn trả hàng
    public function refundProduct($id) {
        $order = Order::find($id);
        $order->load('orderDetails', 'orderDetails.product');
        $product_buy = [];
        foreach ($order->orderDetails as $detail) {
            $count_exchange_amount = 0;
            $exchanges = Exchange::where('id_order_detail', $detail->id);
            foreach ($exchanges as $item) {
                $count_exchange_amount += $item->amount;
            }
            if ($detail->quantity > $count_exchange_amount)
                $product_buy[$detail->id."_".$detail->product->id] = $detail->product->name;
        }

        return view('admin.transaction.refund_product',[
            'order_id' => $id,
            'product_buy' => $product_buy
        ]);
    }

    public function postRefundProduct($id, Request $request) {
        $quantity_refund = $request->quantity_refund;
        $order = Order::find($id);
        $input = explode('_', $request->product_refund);
        $order_detail = OrderDetail::find($input[0]);
        $product_refund = Product::find($input[1]);
        DB::beginTransaction();
        try{
            $product_refund->amount += $quantity_refund;
            $product_refund->save();
            if ($order_detail->quantity == $quantity_refund){
                $order_detail->delete();
                $order->delete();
            } else {
                $order_detail->quantity -= $quantity_refund;
                $order_detail->save();
                $order->total -= $order_detail->unit_price * $quantity_refund;
                $order->save();
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
        $old_order = Order::find($id);
        if ($old_order) {
            return redirect('admin/transaction/changeStatus/'.$id)->with('thongbao', 'Hoàn trả hàng thành công!');
        } else{
            return redirect('admin/order/view')->with('thongbao', 'Hoàn trả hàng thành công!');
        }
    }

    // xử lí đổi trả cho đơn hàng
    public function exchange($id){
        $order = Order::find($id);
        $order->load('orderDetails', 'orderDetails.product');
        $product_buy = [];
        foreach ($order->orderDetails as $detail) {
            $count_exchange_amount = 0;
            $exchanges = Exchange::where('id_order_detail', $detail->id);
            foreach ($exchanges as $item) {
                $count_exchange_amount += $item->amount;
            }
            if ($detail->quantity > $count_exchange_amount)
            $product_buy[$detail->id."_".$detail->product->id] = $detail->product->name;
        }
        $categories = Catalog::get();
        return view('admin.transaction.exchange',[
            'order_id' => $id,
            'product_buy' => $product_buy,
            'categories' => $categories
        ]);
    }

    public function postExchange($id, Request $request) {
        $payment_amount = 0;
        $refund = explode('_', $request->product_refund);
        $id_order_detail = (int)$refund[0];
        $id_product_refund = (int)$refund[1];
        $order = Order::find($id);
        $order_detail = OrderDetail::find($id_order_detail);
        $product_exchange = Product::find($request->product_exchange);
        $product_refund = Product::find($id_product_refund);
        $unit_price_exchange = $product_exchange->promotion_price ? $product_exchange->promotion_price : $product_exchange->unit_price;
        if ($unit_price_exchange < $order_detail->unit_price) {
            return redirect('admin/transaction/exchange/'.$id)->with('thongbao', 'Không thể thực hiện đổi trả vì sản phẩm đổi có giá trị nhỏ hơn sản phẩm trả');
        } else {
            $payment_amount =  ($unit_price_exchange - $order_detail->unit_price) * $request->quantity_exchange;
        }
        $inputs = [
            'id_order' => $id,
            'id_order_detail' => $id_order_detail,
            'id_product_refund' => $id_product_refund,
            'unit_price_refund' => $order_detail->unit_price,
            'id_product_exchange' => $request->product_exchange,
            'unit_price_exchange' => $unit_price_exchange,
            'amount' => $request->quantity_exchange,
            'payment_amount' => $payment_amount
        ];
        DB::beginTransaction();

        try {
            $new_exchange = Exchange::create($inputs);
            $product_refund->amount = $product_refund->amount + $request->quantity_exchange;
            $product_refund->save();
            $product_exchange->amount_ordered = $product_exchange->amount_ordered + $request->quantity_exchange;
            $product_exchange->save();
            $order->status = 4;
            $order->save();
            DB::commit();
        } catch (\Exception $e) {
           DB::rollBack();
        }
        return redirect('admin/transaction/changeStatus/'.$id)->with('Tạo đơn đổi trả hàng thành công!');
    }

    //xóa một giao dịch
    public function delete($id){
        $tran = Order::find($id);
        if(!$tran) return view('admin.product.error');
        OrderDetail::where('id_bill',$id)->delete();
        $tran->delete();
        return redirect('admin/transaction/view')->with('thongbao','Xóa giao dịch thành công');
    }
    //xóa nhiều giao dịch
    public function deleteMultiple(Request $request){
        foreach ($request->allVals as $row){
            $tran = Order::find($row);
            if(!$tran) return response()->json('fail');
            OrderDetail::where('id_bill',$row)->delete();
            $tran->delete();
        }
        return response()->json('ok');
    }
    //tìm kiếm transaction
    public function search(Request $request){
        $date_from = null;
        $date_to = null;
        $order_id = null;
        $bill_id = null;
        $input = $request->all();
        $query = Order::query();
        if ($input['bill_id']) {
            $bill_id = (int)$input['bill_id'];
            $query->with(['bill' => function ($q) use ($bill_id) {
                $q->where('id', $bill_id);
            }]);
        }
        if ($input['date_from']) {
            $query->where('created_at', '>=', date('Y-m-d',strtotime($input['date_from'])));
            $date_from = $input['date_from'];
        }
        if ($input['date_to']) {
            $query->where('created_at', '<=', date('Y-m-d',strtotime($input['date_to'])));
            $date_to = $input['date_to'];
        }
        if ($input['order_id'] && (int)$input['order_id']) {
            $query->where('id', (int)$input['order_id']);
            $order_id = $input['order_id'];
        }
        if (!$input['date_from'] && !$input['date_to'] && !$input['order_id'] && !$input['bill_id']) {
            return redirect('admin/transaction/view');
        }
        $orders = $query
            ->where('status',5)
            ->orderBy('created_at', 'DESC')
            ->get();
        return view('admin.transaction.view',[
            'tran' => $orders,
            'date_from' => $date_from,
            'date_to' => $date_to,
            'order_id' => $order_id,
            'bill_id' => $bill_id
        ]);
    }
}
