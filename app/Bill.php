<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bill extends Model
{
    public $table = 'bills';
    protected $fillable = ['order_id', 'user_id', 'total', 'received_date'];

    public function order() {
        return $this->belongsTo(Order::class, 'order_id');
    }
}
