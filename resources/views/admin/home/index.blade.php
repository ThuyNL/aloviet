@extends('admin.layout')
@section('content')
    <div class="wrapper">

        <div style="margin-top: 50px" class="widgets">
            <!-- Stats -->
            <!-- User -->
            <div class="oneTwo">
                <div class="widget">
                    <div class="title">
                        <img src="source/backend/admin/images/icons/dark/users.png" class="titleIcon" />
                        <h6>Thống kê dữ liệu</h6>
                    </div>

                    <table cellpadding="0" cellspacing="0" width="100%" class="sTable myTable">
                        <tbody>

                        <tr>
                            <td>
                                <div class="left">Tổng số đơn hàng thành công</div>
                                <div class="right f11"><a href="admin/transaction/view" target="_blank">Chi tiết</a></div>
                            </td>

                            <td class="textC webStatsLink">
                                {{$total_tran}}					</td>
                        </tr>

                        <tr>
                            <td>
                                <div class="left">Tổng số sản phẩm đang bán</div>
                                <div class="right f11"><a href="admin/product/view" target="_blank">Chi tiết</a></div>
                            </td>

                            <td class="textC webStatsLink">
                                {{$total_product}}					</td>
                        </tr>

                        <tr>
                            <td>
                                <div class="left">Tổng số yêu cầu đặt hàng trước</div>
                                <div class="right f11"><a href="admin/contact/view" target="_blank">Chi tiết</a></div>
                            </td>

                            <td class="textC webStatsLink">
                                {{$total_comment}}					</td>
                        </tr>

                        <tr>
                            <td>
                                <div class="left">Tổng số khách hàng</div>
                                <div class="right f11"><a href="admin/user/view" target="_blank">Chi tiết</a></div>
                            </td>

                            <td class="textC webStatsLink">
                                {{$total_user}}					</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="clear"></div>

            <!-- Giao dich thanh cong gan day nhat -->
        </div>

    </div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.chitiet').click(function (e) {
                e.preventDefault();
                $.ajax({
                    type:'get',
                    url:'admin/transaction/chitiet/'+$(this).attr('value'),
                    success:function (data) {
                        var str = '<table cellpadding="0" cellspacing="0" ' +
                            'width="100%" class="sTable mTable myTable" id="checkAll">'+
                            '<thead>\n' +
                            '                <tr>\n' +
                            '                    <td style="width:60px;">Mã số đơn hàng</td>\n' +
                            '                    <td>Sản phẩm</td>\n' +
                            '\n' +
                            '                    <td style="width:70px;">Số tiền</td>\n' +
                            '                    <td style="width:50px;">Số lượng</td>\n' +
                            '                    <td style="width:75px;">Đơn hàng</td>\n' +
                            '                    <td style="width:75px;">Ngày tạo</td>\n' +
                            '                </tr>\n' +
                            '                </thead>'+
                            ' <tbody class="list_item">\n';
                        data.forEach(function (value) {
                            var status;
                            if (value.status == null) {
                                status = 'Chờ xử lý';
                            } else if (value.status == 1) {
                                status = "Đã tiếp nhận"
                            } else if (value.status == 2) {
                                status = "Đang giao hàng"
                            } else if (value.status == 3) {
                                status = "Thành công"
                            }
                            str +='<tr><td class="textC">'+value.id_order+'</td>\n'+
                                ' <td>\n' +
                                '                        <div class="image_thumb">\n' +
                                '                            <img src="source/image/product/'+value.image+'" height="70">\n' +
                                '                        </div>\n' +
                                '                        <div style="margin-top: 17px">\n' +
                                '                            <a href="'+value.id_product+'/chitiet" class="tipS" title="" target="_blank">\n' +
                                '                                <b>'+value.name+'</b>\n' +
                                '                            </a>\n' +
                                '                        </div>\n' +
                                '                        <div class="clear"></div>\n' +
                                '\n' +
                                '                    </td>\n' +
                                '\n' +
                                '                    <td class="textC">\n' +
                                '                        '+value.unit_price+' đ\n' +
                                '                    </td>\n' +
                                '\n' +
                                '                    <td class="textC">'+value.quantity+'</td>\n' +
                                '\n' +
                                '                    <td class="status textC">\n' +
                                '\t\t\t\t\t\t<span class="pending">\n' +
                                '\t\t\t\t\t\t'+status+'\t\t\t\t\t\t</span>\n' +
                                '                    </td>\n' +
                                '\n' +
                                '                    <td class="textC">'+value.created_at+'</td></tr>'
                        });
                        str+='</tbody></table>';

                        $.dialog({
                            theme: 'material',
                            title: '',
                            content: str,
                            animationSpeed: 100,
                            backgroundDismiss: true,
                        });
                    }
                });
            });
        });
    </script>
@endsection