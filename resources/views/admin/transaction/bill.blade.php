@extends('admin.layout')
@section('content')
    <div class="titleArea">
        <div class="wrapper">
            <div class="pageTitle">
                <h5>Thông tin hóa đơn</h5>
                <span>Quản lý đơn hàng thành công</span>
            </div>
            <div class="horControlB menu_action">
                <ul>

                    <li><a href="admin/transaction/view">
                            <img src="source/backend/admin/images/icons/control/16/list.png" />
                            <span>Danh sách đơn hàng thành công</span>
                        </a></li>
                    <li style="pointer-events:none;opacity:0.6;"><a href="admin/transaction/view">
                            <img src="source/backend/admin/images/excel.png" />
                            <span>Xuất file excel</span>
                        </a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="line"></div>
    <!-- Message -->

    <!-- Main content wrapper -->
    <div class="wrapper">

        <!-- Form -->
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <fieldset>
                <div class="widget">
                    <div class="formRow" style="background-color: #1BA39C">
                        <h6 style="color: #ffffff;">HÓA ĐƠN MÃ SỐ {{$order->bill->id}}</h6>
                    </div>
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Mã hóa đơn:</label>
                        <div class="formRight">
                            <span class="oneTwo">{{ $order->bill->id}}</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Mã đơn hàng:</label>
                        <div class="formRight">
                            <span class="oneTwo">{{ $order->id}}</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Tên khách hàng:</label>
                        <div class="formRight">
                            <span class="oneTwo">{{ $order->name }}</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Số điện thoại:</label>
                        <div class="formRight">
                            <span class="oneTwo">{{ $order->phone }}</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Địa chỉ:</label>
                        <div class="formRight">
                            <span class="oneTwo">{{ $order->address }}</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                   @foreach($order->order_details as $key => $item)
                    <div class="formRow">
                        @if(count($order->order_details) > 1)
                        <label class="formLeft" for="param_name">Tên sản phẩm {{$key + 1}}:</label>
                        @else
                            <label class="formLeft" for="param_name">Tên sản phẩm:</label>
                        @endif
                            <div class="formRight">
                            <span class="oneTwo">{{ $item->product->name }}</span>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <!-- Price -->
                    <div class="formRow">
                        <label class="formLeft" for="param_price">
                            Giá bán:
                        </label>
                        <div class="formRight">
                            <span class="oneTwo format_number">{{ $item->unit_price }}</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <!-- Price -->
                    <div class="formRow">
                        <label class="formLeft" for="param_price">
                            Số lượng:
                        </label>
                        <div class="formRight">
                            <span class="oneTwo">{{ $item->quantity}}</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    @endforeach
                    <!-- Price -->
                       <div class="formRow">
                           <label class="formLeft" for="param_price">
                               Tổng tiền:
                           </label>
                           <div class="formRight">
                            <span class="oneTwo format_number">{{ $order->total}}</span>
                           </div>
                           <div class="clear"></div>
                       </div>
                           <div class="formRow">
                               <label class="formLeft" for="time_exchange">
                                   Ngày nhận hàng:
                               </label>
                               <div class="formRight">
                            <span class="oneTwo">
{{--                                <input name="time_exchange" value="{{ $remain_time}}" style='width:100px' id="time_exchange" type="text" disabled/>--}}
                                {{Carbon\Carbon::parse($order->received_date)->format('d-m-Y')}}
                            </span>
                               </div>
                               <div class="clear"></div>
                           </div>
                        </div>
            </fieldset>
        </form>
    </div>
    <div class="clear mt30"></div>
@endsection