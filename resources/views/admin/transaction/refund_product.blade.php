@extends('admin.layout')
@section('content')
    <div class="titleArea">
        <div class="wrapper">
            <div class="pageTitle">
                <h5>Trả hàng</h5>
                <span>Hoàn trả sản phẩm của đơn hàng</span>
            </div>
            <div class="horControlB menu_action">
                <ul>
                    <li><a href="admin/transaction/changeStatus/<?php echo ($order_id)?>">
                            <img src="source/backend/admin/crown/images/icons/notifications/information.png">
                            <span>Thông tin chi tiết đơn hàng</span>
                        </a></li>
                    <li><a href="admin/order/view">
                            <img src="source/backend/admin/images/icons/control/16/list.png" />
                            <span>Danh sách</span>
                        </a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="line"></div>
    <!-- Message -->

    <!-- Main content wrapper -->
    <div class="wrapper">

        <!-- Form -->
        <form style="width: 50%" class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <fieldset>
                <div class="widget">
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Mã đơn hàng:</label>
                        <div class="formRight">
                            <span class="oneTwo"><input style='width:150px; font-weight: bold' name="order_id" value="#{{ $order_id }}" _autocheck="true" type="text" disabled /></span>
                            <span name="order_id_autocheck" class="autocheck"></span>
                            <div name="order_id_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label class="formLeft" for="param_name">Sản phẩm trả:<span class="req">*</span></label>
                        <div class="formRight">
                            <select name="product_refund" id="product_refund" style="width: 150px" class="left" required>
                                <option value="" selected disabled>Chọn sản phẩm...</option>
                                @foreach($product_buy as $id => $product)
                                    <option value="<?php echo $id; ?>">{{$product}}</option>
                                @endforeach
                            </select>
                            <span name="id_product_autocheck" class="autocheck"></span>
                            <div name="id_product_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <!-- Price -->
                    <div class="formRow">
                        <label class="formLeft" for="quantity_exchange">
                            Số lượng trả:<span class="req">*</span>
                        </label>
                        <div class="formRight">
                            <select name="quantity_refund" id="quantity_refund" style="width: 150px" class="left" required>
                                <option value="" selected disabled>Chọn số lượng...</option>
                            </select>
                            <span name="id_product_autocheck" class="autocheck"></span>
                            <div name="id_product_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                    <div style="align-items: center" class="formSubmit">
                        <input type="submit" id="refund_product" value="Hoàn trả" class="redB" />
                    </div>
                    <div class="clear"></div>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="clear mt30"></div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('#product_refund').on('change', function (e) {
                e.preventDefault();
                var option = $(this).val();
                if (option) {
                    $.ajax({
                        type: 'get',
                        url: 'admin/product/getQuantity/'+option,
                        success: function (data) {
                            $('#quantity_refund').empty();
                            for (i = 1; i <= data; i++) {
                                $('#quantity_refund')
                                    .append($("<option></option>")
                                        .attr("value",i)
                                        .text(i));
                            }
                        }
                    });
                }
            });
        });
    </script>
@endsection