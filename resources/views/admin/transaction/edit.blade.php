@extends('admin.layout')
@section('content')
    <div class="titleArea">
        <div class="wrapper">
            <div class="pageTitle">
                <h5>Thông tin chi tiết</h5>
                <span>Quản lý sản phẩm</span>
            </div>
            <div class="horControlB menu_action">
                <ul>

                    <li><a href="admin/order/view">
                            <img src="source/backend/admin/images/icons/control/16/list.png" />
                            <span>Danh sách</span>
                        </a></li>
                    <li style="pointer-events:none;opacity:0.6;"><a href="admin/order/view">
                            <img src="source/backend/admin/images/excel.png" />
                            <span>Xuất file excel</span>
                        </a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="line"></div>
    <!-- Message -->

    <!-- Main content wrapper -->
    <div class="wrapper">

        <!-- Form -->
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <fieldset>
                <div class="widget">
                    <div class="formRow" style="background-color: #1BA39C">
                        <h6 style="text-align: center; color: #ffffff;">THÔNG TIN ĐƠN HÀNG</h6>
                    </div>
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Mã đơn hàng:</label>
                        <div class="formRight">
                            <span class="oneTwo"><input name="id_order" style="width: 100px; font-weight: bold" value="#{{ $order->id}}" id="param_order_id" _autocheck="true" type="text" disabled /></span>
                            <span name="name_autocheck" class="autocheck"></span>
                            <div name="name_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Tên khách hàng:</label>
                        <div class="formRight">
                            <span class="oneTwo">{{ $order->name }}</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Số điện thoại:</label>
                        <div class="formRight">
                            <span class="oneTwo">{{ $order->phone }}</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Địa chỉ:</label>
                        <div class="formRight">
                            <span class="oneTwo">{{ $order->address }}</span>
                        </div>
                        <div class="clear"></div>
                    </div>
                   @foreach($order->order_details as $index => $item)
                    <div class="formRow">
                        @if(count($order->order_details) >1)
                        <label class="formLeft" for="param_name">Tên sản phẩm {{$index+1}}:</label>
                        @else
                            <label class="formLeft" for="param_name">Tên sản phẩm:</label>
                        @endif
                            <div class="formRight">
                            <span class="oneTwo"><input name="name" value="{{ $item->product->name }}" id="param_name" _autocheck="true" type="text" disabled /></span>
                            <span name="name_autocheck" class="autocheck"></span>
                            <div name="name_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                        <div class="formRow">
                            <label class="formLeft" for="id_product">Mã sản phẩm:</label>
                            <div class="formRight">
                                <span class="oneTwo"><input name="id_product" style="width: 100px;" value="#{{ $item->product->id }}" id="param_name" _autocheck="true" type="text" disabled /></span>
                                <span name="name_autocheck" class="autocheck"></span>
                                <div name="name_error" class="clear error"></div>
                            </div>
                            <div class="clear"></div>
                        </div>

                    <!-- Price -->
                    <div class="formRow">
                        <label class="formLeft" for="param_price">
                            Giá bán tại thời điểm đặt hàng:
                        </label>
                        <div class="formRight">
                            <span class="oneTwo">
                                <input name="price" value="{{ $item->unit_price }}" style='width:100px' id="param_price" class="format_number" _autocheck="true" type="text" disabled/>
                                <img class='tipS' title='Giá bán sử dụng để giao dịch' style='margin-bottom:-8px'  src='source/backend/admin/crown/images/icons/notifications/information.png'/>
                            </span>
                            <span name="price_autocheck" class="autocheck"></span>
                            <div name="price_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

{{--                    @if($item->product->promotion_price)--}}
{{--                        <div class="formRow">--}}
{{--                            <label class="formLeft" for="param_price">--}}
{{--                                Giảm giá:--}}
{{--                            </label>--}}
{{--                            <div class="formRight">--}}
{{--                            <span class="oneTwo">--}}
{{--                                <input name="promotion_price" value="{{ $item->product->promotion_price }}" style='width:100px' id="param_price" class="format_number" _autocheck="true" type="text" disabled/>--}}
{{--                                <img class='tipS' title='Giá bán sử dụng để giao dịch' style='margin-bottom:-8px'  src='source/backend/admin/crown/images/icons/notifications/information.png'/>--}}
{{--                            </span>--}}
{{--                                <span name="price_autocheck" class="autocheck"></span>--}}
{{--                                <div name="price_error" class="clear error"></div>--}}
{{--                            </div>--}}
{{--                            <div class="clear"></div>--}}
{{--                        </div>--}}
{{--                        @endif--}}
                    <!-- Price -->
                    <div class="formRow">
                        <label class="formLeft" for="param_price">
                            Số lượng:
                        </label>
                        <div class="formRight">
                            <span class="oneTwo">
                                <input name="quantity" value="{{ $item->quantity}}" style='width:100px' id="param_price" class="format_number" _autocheck="true" type="text" disabled/>
                            </span>
                            <span name="price_autocheck" class="autocheck"></span>
                            <div name="price_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    @endforeach
                    <!-- Price -->
                       <div class="formRow">
                           <label class="formLeft" for="param_price">
                               Tổng tiền:
                           </label>
                           <div class="formRight">
                            <span class="oneTwo">
                                <input name="total" value="{{ $order->total}}" style='width:100px' id="param_price" class="format_number" _autocheck="true" type="text" disabled/>
{{--                                <img class='tipS' title='Giá bán sử dụng để giao dịch' style='margin-bottom:-8px'  src='source/backend/admin/crown/images/icons/notifications/information.png'/>--}}
                            </span>
                               <span name="price_autocheck" class="autocheck"></span>
                               <div name="price_error" class="clear error"></div>
                           </div>
                           <div class="clear"></div>
                       </div>

                    <div class="formRow">
                        <label class="formLeft" for="param_cat">Trạng thái:</label>
                        <div class="formRight">
                            <select name="status_order" style="width: 105px;" id='status_order' class="left">
                                    <option value="0" <?php if($order->status ==0) { echo 'selected'; } else {echo 'disabled';}?>>Chờ xử lý</option>
                                    <option value="1" <?php if($order->status ==1) { echo 'selected disabled'; }
                                    if($order->status == 2 || $order->status == 3 || $order->status == 4) {echo 'disabled';}?>>Đã đóng gói</option>
                                    <option value="2" <?php if($order->status ==2) { echo 'selected disabled'; }
                                        if($order->status == 3 || $order->status == 4 || $order->status == 0) {echo 'disabled';}?>>Đang giao hàng</option>
                                    <option value="3" <?php if($order->status ==3) { echo 'selected disabled'; }
                                    if($order->status == 4 || $order->status == 0){ echo 'disabled'; } ?>>Đã nhận hàng</option>
                                @if($order->status == 4)
                                    <option value="4" <?php if($order->status ==4) { echo 'selected disabled'; }?>>Đổi trả hàng</option>
                                    @endif
                            </select>
                            <span name="cat_autocheck" ></span>
                            <div name="cat_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>
                       @if($order->received_date)
                           <div class="formRow">
                               <label class="formLeft" for="time_exchange">
                                   Ngày nhận hàng:
                               </label>
                               <div class="formRight">
                            <span class="oneTwo">
{{--                                <input name="time_exchange" value="{{ $remain_time}}" style='width:100px' id="time_exchange" type="text" disabled/>--}}
                                {{Carbon\Carbon::parse($order->received_date)->format('d-m-Y')}}
                            </span>
                               </div>
                               <div class="clear"></div>
                           </div>
                       @endif

                    @if(isset($remain_time))
                       <div class="formRow">
                           <label class="formLeft" for="time_exchange">
                                Thời gian đổi trả còn lại:
                           </label>
                           <div class="formRight">
                            <span class="oneTwo">
{{--                                <input name="time_exchange" value="{{ $remain_time}}" style='width:100px' id="time_exchange" type="text" disabled/>--}}
                                {{ $remain_time}} ngày
                            </span>
                           </div>
                           <div class="clear"></div>
                       </div>
                    @endif

                       @if(count($order->exchanges) > 0)
                           @foreach($order->exchanges as $index => $exchange)
                            <div class="formRow" style="background-color: #1BA39C">
                                <h6 style="text-align: center; color: #ffffff;">ĐƠN ĐỔI TRẢ {{$index+1}}</h6>
                            </div>
                            <div class="formRow">
                                <label class="formLeft" for="param_name">Mã đổi trả:</label>
                                <div class="formRight">
                                    <span class="oneTwo"><input style="width: 100px; font-weight: bold;" name="exchange_id" value="#{{ $exchange->id }}" id="param_exchange_id" _autocheck="true" type="text" disabled /></span>
                                    <span name="name_autocheck" class="autocheck"></span>
                                    <div name="name_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="formRow">
                                <label class="formLeft" for="param_name">Tên sản phẩm trả:</label>
                                <div class="formRight">
                                    <span class="oneTwo"><input name="id_product_refund" value="{{ $exchange->productRefund->name }}" id="id_product_refund" _autocheck="true" type="text" disabled /></span>
                                    <span name="name_autocheck" class="autocheck"></span>
                                    <div name="name_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="formRow">
                                <label class="formLeft" for="param_name">Giá bán của sản phẩm được trả:</label>
                                <div class="formRight">
                                    <span class="oneTwo"><input name="id_product_refund" value="{{ $exchange->unit_price_refund }}" id="price_product_refund" class="format_number" _autocheck="true" type="text" disabled /></span>
                                    <span name="name_autocheck" class="autocheck"></span>
                                    <div name="name_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="formRow">
                                <label class="formLeft" for="param_name">Tên sản phẩm đổi:</label>
                                <div class="formRight">
                                    <span class="oneTwo"><input name="id_product_exchange" value="{{ $exchange->productExchange->name }}" id="id_product_exchange" _autocheck="true" type="text" disabled /></span>
                                    <span name="name_autocheck" class="autocheck"></span>
                                    <div name="name_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="formRow">
                                <label class="formLeft" for="param_name">Giá bán của sản phẩm được đổi:</label>
                                <div class="formRight">
                                    <span class="oneTwo"><input name="id_product_refund" value="{{ $exchange->unit_price_exchange }}" id="price_product_exchange" class="format_number" _autocheck="true" type="text" disabled /></span>
                                    <span name="name_autocheck" class="autocheck"></span>
                                    <div name="name_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="formRow">
                                <label class="formLeft" for="param_name">Số lượng đổi trả:</label>
                                <div class="formRight">
                                    <span class="oneTwo"><input style="width: 100px" name="amount_exchange" value="{{ $exchange->amount }}" id="amount_exchange" _autocheck="true" type="text" disabled /></span>
                                    <span name="name_autocheck" class="autocheck"></span>
                                    <div name="name_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="formRow">
                                <label class="formLeft" for="param_name">Tổng tiền cần thanh toán:</label>
                                <div class="formRight">
                                    <span class="oneTwo"><input style="width: 100px" name="payment_amount" value="{{ $exchange->payment_amount}}" id="payment_amount" class="format_number" _autocheck="true" type="text" disabled /></span>
                                    <span name="name_autocheck" class="autocheck"></span>
                                    <div name="name_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            <div class="formRow">
                                <label class="formLeft" for="param_cat">Trạng thái:</label>
                                <div class="formRight">
                                    <select style="width: 105px;" name="status_exchange_<?php echo $exchange->id?>" id='status' class="left">
                                        <option value="0" <?php if($exchange->status ==0) { echo 'selected'; } else {echo 'disabled';}?>>Chờ xử lý</option>
                                        <option value="1" <?php if($exchange->status ==1) { echo 'selected disabled'; }
                                            if($exchange->status == 2 || $exchange->status == 3) {echo 'disabled';}?>>Đã đóng gói</option>
                                        <option value="2" <?php if($exchange->status ==2) { echo 'selected disabled'; }
                                            if($exchange->status == 3 || $exchange->status == 0) {echo 'disabled';}?>>Đang giao hàng</option>
                                        <option value="3" <?php if($exchange->status ==3) { echo 'selected disabled'; }
                                        if ($exchange->status == 0) {echo 'disabled';}?>>Đã nhận hàng</option>
                                    </select>
                                    <span name="cat_autocheck" ></span>
                                    <div name="cat_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                            @if($exchange->received_date)
                                <div class="formRow">
                                    <label class="formLeft" for="time_exchange">
                                        Ngày nhận hàng:
                                    </label>
                                    <div class="formRight">
                            <span class="oneTwo">
{{--                                <input name="time_exchange" value="{{ $remain_time}}" style='width:100px' id="time_exchange" type="text" disabled/>--}}
                                {{Carbon\Carbon::parse($exchange->received_date)->format('d-m-Y')}}
                            </span>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                                @endif
                               @endforeach
                       @endif
                </div>
                <div class="formSubmit">
                    @if($order->status != 3 && $is_updated && !$is_success)
                        <input type="submit" id="add" value="CẬP NHẬT TRẠNG THÁI" class="redB"/>
                    @endif
                    @if($order->status != 3 && $order->status != 4)
                        {{--                        <button><a class="btn btn-default" href="#modal-confirm" data-toggle="modal" data-target="#modal-confirm" data-url="{!! URL::route('refund', ['id' => $order->id]) !!}">HOÀN ĐƠN</a></button>--}}
                        <button class="btn-refund" type="button" style="color: #2b6893"><a href="{{route('refund_order', ['id' => $order->id])}}">HỦY ĐƠN HÀNG</a></button>
                    @endif
                    @if($exchangable)
                            <button type="button" class="btn-add" id="exchange"><a href="{{route('exchange', ['id' => $order->id])}}">ĐỔI HÀNG</a></button>
                            <button type="button" class="btn-add" id="exchange"><a href="{{route('refund_product', ['id' => $order->id])}}">TRẢ HÀNG</a></button>
                        @endif
                        @if($is_success)
                        <button type="button" class="redB" id="export_bill"><a style="color: #ffffff" href="{{route('export_bill', ['id' => $order->id])}}">XUẤT HÓA ĐƠN</a></button>
                    @endif

                </div>
                <div class="clear"></div>
            </fieldset>
        </form>
    </div>
    <div class="clear mt30"></div>
@endsection