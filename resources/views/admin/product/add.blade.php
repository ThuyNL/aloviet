@extends('admin.layout')
@section('content')
    <div class="titleArea">
        <div class="wrapper">
            <div class="pageTitle">
                <h5>Sản phẩm</h5>
                <span>Quản lý sản phẩm</span>
            </div>

            <div class="horControlB menu_action">
                <ul>
                    <li><a href="admin/product/add">
                            <img src="source/backend/admin/images/icons/control/16/add.png" />
                            <span>Thêm mới</span>
                        </a></li>
                    <li><a href="admin/product/view">
                            <img src="source/backend/admin/images/icons/control/16/list.png" />
                            <span>Danh sách</span>
                        </a></li>

                </ul>
            </div>

            <div class="clear"></div>
        </div>
    </div>
    <div class="line"></div>
    <!-- Message -->

    <!-- Main content wrapper -->
    <div class="wrapper">

        <!-- Form -->
        <form class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <fieldset>
                <div class="widget">
                    <div class="title">
                        <img src="source/backend/admin/images/icons/dark/add.png" class="titleIcon" />
                        <h6>Thêm mới Sản phẩm</h6>
                    </div>

                            <div class="formRow">
                                <label class="formLeft" for="param_name">Tên:<span class="req">*</span></label>
                                <div class="formRight">
{{--                                    <span class="oneTwo"><input name="name" value="{{old('name')}}" id="param_name" _autocheck="true" type="text" required /></span>--}}
                                    <span class="oneTwo"><input name="name" value="" id="param_name" _autocheck="true" type="text" required /></span>
                                    <span name="name_autocheck" class="autocheck"></span>
                                    <div name="name_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="formRow">
                                <label class="formLeft">Hình ảnh:<span class="req">*</span></label>
                                <div class="formRight">
                                    <div class="left"><input type="file"  id="image" name="image" required /></div>
                                    <div name="image_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                    <!-- amount -->
                    <div class="formRow">
                        <label class="formLeft" for="amount">
                            Số lượng :
                            <span class="req">*</span>
                        </label>
                        <div class="formRight">
		<span class="oneTwo">
{{--			<input name="price" value="{{old('price')}}" style='width:100px' id="param_price" class="format_number" _autocheck="true" type="text" required/>--}}
			<input name="amount" value="" style='width:100px' id="amount" class="format_number" type="text" required/>
		</span>
                            <span name="amount_autocheck" class="autocheck"></span>
                            <div name="amount_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <!-- Price -->
                    <div class="formRow">
                        <label class="formLeft" for="price_import">
                            Giá nhập :
                            <span class="req">*</span>
                        </label>
                        <div class="formRight">
		<span class="oneTwo">
{{--			<input name="price" value="{{old('price')}}" style='width:100px' id="param_price" class="format_number" _autocheck="true" type="text" required/>--}}
			<input name="price_import" value="" style='width:100px' id="price_import" class="format_number" type="text" required/>
		</span>
                            <span name="price_import_autocheck" class="autocheck"></span>
                            <div name="price_import_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                            <!-- Price -->
                            <div class="formRow">
                                <label class="formLeft" for="param_price">
                                    Giá bán :
                                    <span class="req">*</span>
                                </label>
                                <div class="formRight">
		<span class="oneTwo">
{{--			<input name="price" value="{{old('price')}}" style='width:100px' id="param_price" class="format_number" _autocheck="true" type="text" required/>--}}
			<input name="price" value="" style='width:100px' id="param_price" class="format_number" _autocheck="true" type="text" required/>
			<img class='tipS' title='Giá bán sử dụng để giao dịch' style='margin-bottom:-8px'  src='source/backend/admin/crown/images/icons/notifications/information.png'/>
		</span>
                                    <span name="price_autocheck" class="autocheck"></span>
                                    <div name="price_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <!-- Price -->
                            <div class="formRow">
                                <label class="formLeft" for="param_discount">
                                    Giảm giá (VNĐ)
                                    <span></span>:
                                </label>
                                <div class="formRight">
		<span>
{{--			<input name="discount" value="{{old('discount')}}" style='width:100px' id="param_discount" class="format_number"  type="text" />--}}
			<input name="discount" value="" style='width:100px' id="param_discount" class="format_number"  type="text" />
			<img class='tipS' title='Số tiền giảm giá' style='margin-bottom:-8px'  src='source/backend/admin/crown/images/icons/notifications/information.png'/>
		</span>
                                    <span name="discount_autocheck" class="autocheck"></span>
                                    <div name="discount_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>


                            <div class="formRow">
                                <label class="formLeft" for="param_cat">Danh mục:<span class="req">*</span></label>
                                <div class="formRight">
                                    <select name="cat" _autocheck="true" id='param_cat' class="left">
                                        @foreach($loaisp as $row)
{{--                                            @if(old('cat') == $row->id)--}}
{{--                                                <option value="{{$row->id}}" selected="selected">{{$row->name}}</option>--}}
{{--                                            @else--}}
                                                <option value="{{$row->id}}">{{$row->name}}</option>
{{--                                            @endif--}}
                                        @endforeach

                                    </select>
                                    <span name="cat_autocheck" class="autocheck"></span>
                                    <div name="cat_error" class="clear error"></div>
                                </div>
                                <div class="clear"></div>
                            </div>

                            <div class="formRow">
                                <label class="formLeft" for="param_name">Dòng máy:<span class="req">*</span></label>
                                <div class="formRight">
{{--                                        @if(old('new')==1)--}}
{{--                                            <span class="oneFour">Có<input name="new" value="1" type="radio" checked="checked" /></span>--}}
{{--                                            <span>Không<input name="new" value="0" type="radio" /></span>--}}
{{--                                        @else--}}
                                            <span class="oneFour">2 năm gần đây<input name="new" value="1" type="radio"  checked="checked" /></span>
                                            <span>2 năm về trước<input name="new" value="0" type="radio" /></span>
{{--                                        @endif--}}
                                </div>
                                <div class="clear"></div>
                            </div>

                        <div class="formRow">
                            <label class="formLeft">Chi tiết sản phẩm:</label>
                            <div class="form-right-bottom">
                                    <span class="specify">
                                        <label for="screen">Màn hình:</label>
                                        <input type="text" name="screen" class="txt-normal"/>
                                    </span>

                                    <span class="specify">
                                        <label for="system">Hệ điều hành:</label>
                                        <input type="text" name="system" class="txt-normal" />
                                    </span>

                                    <span class="specify">
                                        <label for="camera_after">Camera sau:</label>
                                        <input type="text" name="camera_after" class="txt-normal" />
                                    </span>

                                    <span class="specify">
                                        <label for="camera_before">Camera trước:</label>
                                        <input type="text" name="camera_before" class="txt-normal"/>
                                    </span>

                                    <span class="specify">
                                        <label for="cpu">CPU:</label>
                                        <input type="text" name="cpu" class="txt-normal" />
                                    </span>

                                    <span class="specify">
                                        <label for="ram">RAM:</label>
                                        <input type="text" name="ram" class="txt-normal" />
                                    </span>

                                    <span class="specify">
                                        <label for="cache_in">Bộ nhớ trong:</label>
                                        <input type="text" name="cache_in" class="txt-normal" />
                                    </span>

                                    <span class="specify">
                                        <label for="cache_tag">Thẻ nhớ:</label>
                                        <input type="text" name="cache_tag" class="txt-normal"/>
                                    </span>

                                    <span class="specify">
                                        <label for="sim_tag">Thẻ SIM:</label>
                                        <input type="text" name="sim_tag" class="txt-normal"/>
                                    </span>

                                    <span class="specify">
                                        <label for="description">Mô tả:</label>
                                        <textarea name="description" rows="5"></textarea>
                                    </span>
{{--                                <span class="oneTwo">--}}
{{--                                        <textarea name="mota" id="param_mota" rows="5" required>{{old('mota')}}</textarea>--}}
{{--                                        <label for="param_screen">hệ điều hành</label>--}}
{{--                                        <input type="text" name="param_screen" />--}}

{{--                                    </span>--}}
{{--                                <span class="autocheck" name="name_autocheck"></span>--}}
{{--                                <div class="clear error" name="param_mota"></div>--}}
                            </div>
                            <div class="clear"></div>
                        </div>

                    <div class="formSubmit">
                        <input type="submit" id="add" value="Thêm mới" class="redB" />
                        <input type="reset" value="Hủy bỏ" class="basic" />
                    </div>
                    <div class="clear"></div>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="clear mt30"></div>
@endsection
{{--@section('script')--}}
{{--    <script>--}}
{{--        (function($)--}}
{{--        {--}}
{{--            $(document).ready(function () {--}}
{{--                jQuery.validator.addMethod("fullname", function(value) {--}}
{{--                    return /^[a-zA-Z]/.test(value);--}}
{{--                }, "Tên không hợp lệ");--}}
{{--                $('#form').validate({--}}
{{--                    rules:{--}}
{{--                        image:{--}}
{{--                            required:true,--}}
{{--                        },--}}
{{--                        name:{--}}
{{--                            required:true,--}}
{{--                            minlength:5,--}}
{{--                            fullname:true--}}
{{--                        },--}}
{{--                        amount: {--}}
{{--                            required:true,--}}
{{--                            number:true--}}
{{--                        },--}}
{{--                        price_import:{--}}
{{--                            required:true,--}}
{{--                            number:true--}}
{{--                        },--}}
{{--                        price:{--}}
{{--                            required:true,--}}
{{--                            number:true--}}
{{--                        },--}}
{{--                        discount:{--}}
{{--                            number:true--}}
{{--                        }--}}
{{--                    }--}}
{{--                });--}}
{{--                $('#add').click(function () {--}}
{{--                    if($('#form').valid()){--}}
{{--                        return true;--}}
{{--                    }--}}
{{--                    return false;--}}
{{--                });--}}
{{--            });--}}
{{--        })(jQuery);--}}
{{--    </script>--}}
{{--@endsection--}}