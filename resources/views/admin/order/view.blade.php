@extends('admin.layout')
@section('content')
    <div class="titleArea">
        <div class="wrapper">
            <div class="pageTitle">
                <h5>Quản lý đơn hàng</h5>
                <span>Quản lý các đơn hàng còn thời hạn đổi trả</span>
            </div>

            <div class="horControlB menu_action">
                <ul>

                    <li><a href="{{route('create_order')}}">
                            <img src="source/backend/admin/images/icons/control/16/add.png" />
                            <span>Tạo đơn mới</span>
                        </a></li>
                    <li><a href="admin/order/view">
                            <img src="source/backend/admin/images/icons/control/16/list.png" />
                            <span>Danh sách</span>
                        </a></li>

                    <li style="pointer-events:none;opacity:0.6;"><a href="admin/order/view">
                            <img src="source/backend/admin/images/excel.png" />
                            <span>Xuất file excel</span>
                        </a></li>
                </ul>
            </div>

            <div class="clear"></div>
        </div>
    </div>
    <div class="line"></div>

    <!-- Message -->
    <!-- Main content wrapper -->
    <div class="wrapper">

        <div class="widget">
            <div class="title">
                <h6>Danh sách đơn hàng</h6>
            </div>
            @if(count($orders)>0)
            <table cellpadding="0" cellspacing="0" width="100%" class="sTable mTable myTable">
                <thead class="filter"><tr><td colspan="12">
                        <form class="list_filter form" action="{{route('search_order')}}" method="get">
                            <table cellpadding="0" cellspacing="0" width="100%"><tbody>

                                <tr>

                                    <td class="label" style="width:60px;"><label for="date_from">Từ ngày</label></td>
                                    <td class="item"><input name="date_from" value="{{$date_from or ''}}" id="filter_created" type="text" class="datepicker" /></td>

                                    <td class="label"><label for="date_to">Đến ngày</label></td>
                                    <td class="item"><input name="date_to" value="{{$date_to or ''}}" id="filter_created_to" type="text" class="datepicker" /></td>

                                    <td class="label">Trạng thái</td>
                                    <td class="item"><select style="width: 110px" name="status" id="filter_status">
                                            <option value="">Chọn trạng thái</option>
                                            @if($status)
                                                <option value="0" <?php if($status == 0) echo 'selected';?>>Chờ xử lý</option>
                                                <option value="1" <?php if($status == 1) echo 'selected';?>>Đã đóng gói</option>
                                                <option value="2" <?php if($status == 2) echo 'selected';?>>Đang giao hàng</option>
                                                <option value="3" <?php if($status == 3) echo 'selected';?>>Đã nhận hàng</option>
                                                <option value="4" <?php if($status == 4) echo 'selected';?>>Đổi trả hàng</option>
                                                @else
                                                <option value="0" >Chờ xử lý</option>
                                                <option value="1">Đã đóng gói</option>
                                                <option value="2">Đang giao hàng</option>
                                                <option value="3">Đã nhận hàng</option>
                                                <option value="4">Đổi trả hàng</option>
                                                @endif
                                        </select></td>

                                    <td class="label">Mã đơn hàng</td>
                                    <td class="item"><input name="order_id" value="{{$order_id or ''}}" id="filter_order_id" type="text"/></td>
                                    <td colspan='2' style='width:60px'>
                                        <input type="submit" id="search" class="button blueB" value="Tìm kiếm" />
{{--                                        <input type="reset" class="basic" value="Reset" onclick="window.location.href = 'admin/transaction/view'; ">--}}
                                    </td>


                                </tr>

                                </tbody></table>
                        </form>
                    </td></tr></thead>

                <thead>
                <tr>
                    <td style="width:60px;">Mã số đơn hàng</td>
                    <td style="width:60px;">Thanh toán</td>
                    <td style="width:60px;">Khách hàng</td>
                    <td style="width:60px;">Số điện thoại</td>
                    <td style="width:60px;">Địa chỉ</td>
                    <td style="width:70px;">Tổng tiền</td>
                    <td style="width:75px;">Trạng thái</td>
                    <td style="width:75px;">Ngày tạo</td>
                    <td style="width:55px;">Hành động</td>
                </tr>
                </thead>

                @if($orders instanceof \Illuminate\Pagination\LengthAwarePaginator)
                <tfoot class="auto_check_pages">
                <tr>
                    <td colspan="10">
                        {{$orders->links()}}
                    </td>
                </tr>
                </tfoot>
                @endif

                <tbody class="list_item">
                @if(count($orders)>0)
                @foreach($orders as $order)

                <tr class='row_{{$order->id}}'>
                    <td class="textC">{{$order->id}}</td>
                    <td class="textC">{{$order->payment}}</td>
                    <td class="textC">
                        {{ $order->name}}
                    </td>
                    <td class="textC">
                        {{ $order->phone}}
                    </td>
                    <td class="textC">
                        {{ $order->address}}
                    </td>



                    <td class="textC">
                        {{number_format($order->total)}} đ
                    </td>

                    <td class="status textC">
						<span class="pending">
                            <?php
                                if ($order->status == 0) {
                                    echo 'Chờ xử lý';
                                } else if ($order->status == 1) {
                                    echo 'Đã đóng gói';
                                }else if ($order->status == 2) {
                                    echo 'Đang giao hàng';
                                } else if($order->status == 3){
                                    echo 'Đã nhận hàng';
                                } else if($order->status == 4) {
                                    echo 'Đổi trả hàng';
                                } else {
                                    echo 'Thành công';
                                }
                            ?>
												</span>
                    </td>

                    <td class="textC">{{$order->created_at}}</td>

                    <td class="textC">
                        <a href="admin/transaction/changeStatus/{{$order->id}}" value="{{$order->id}}" title="Xem chi tiết đơn hàng" class="tipS chitiet" >
                            <img src="source/backend/admin/images/icons/color/view.png" />
                        </a>
                        <a href="admin/tran/del/12" title="Xóa"  value="{{$order->id}}" class="tipS delete" >
                            <img src="source/backend/admin/images/icons/color/delete.png" />
                        </a>
                    </td>

                </tr>
                @endforeach
                @endif
                </tbody>

            </table>
            @else
                <h5 style="margin: 15px">Không có đơn hàng nào</h5>
            @endif
        </div>

    </div>
    <div class="clear mt30"></div>
@endsection
@section('script')
    <script>
        $(document).ready(function () {
            $('.delete').click(function (e) {
                e.preventDefault();
                id = $(this).attr('value');
                $.confirm({
                    theme: 'material',
                    title:'',
                    content: 'Bạn có chắc chắn muốn xóa đơn hàng này',
                    buttons: {
                        Ok: {
                            btnClass: 'btn-blue',
                            action:function () {
                                window.location.href = 'admin/order/delete/'+id;
                            }
                        },
                        Cancel: {}
                    }
                });
            });
            // $('#search').click(function () {
            //     if($('#filter_created').val()&&$('#filter_created_to').val())
            //         return true;
            //     return false;
            // });
            $('#deleteAll').click(function (e) {
                e.preventDefault();
                var allVals = [];
                $(".check-product:checked").each(function() {
                    allVals.push($(this).attr('value'));
                });
                if(allVals.length ==0){
                    $.dialog({
                        theme: 'material',
                        title: '',
                        content: 'Vui lòng chọn đơn hàng muốn xóa',
                        animationSpeed: 200,
                        backgroundDismiss: true,
                    });
                }else{
                    $.confirm({
                        theme: 'material',
                        title:'',
                        content: 'Bạn có chắc chắn muốn xóa các đơn hàng đã chọn',
                        buttons: {
                            Ok: {
                                btnClass: 'btn-blue',
                                action:function () {
                                    $.ajax({
                                        type: "post",
                                        url: 'admin/order/deleteMultiple',
                                        data:{
                                            _token:$('meta[name="csrf-token"]').attr('content'),
                                            allVals:allVals
                                        },
                                        success:function (data) {
                                            window.location.href = 'admin/order/view';
                                            $.dialog({
                                                theme: 'material',
                                                title: '',
                                                content: 'Xóa đơn hàng thành công',
                                                animationSpeed: 100,
                                                backgroundDismiss: true,
                                            });
                                               // window.location.href = 'admin/product/error';
                                            }
                                            // $.each(allVals, function( index, value ) {
                                            //     $('table tr').filter(".row_" + value).remove();
                                            // });
                                    });
                                }
                            },
                            Cancel: {}
                        }
                    });
                }
            });
        });
    </script>
@endsection