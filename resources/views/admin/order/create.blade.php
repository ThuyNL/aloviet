@extends('admin.layout')
@section('content')
    <div class="titleArea">
        <div class="wrapper">
            <div class="pageTitle">
                <h5>Tạo đơn hàng</h5>
                <span>Tạo đơn hàng mới</span>
            </div>
            <div class="horControlB menu_action">
                <ul>
                    <li><a href="admin/order/view">
                            <img src="source/backend/admin/images/icons/control/16/list.png" />
                            <span>Danh sách</span>
                        </a></li>
                </ul>
            </div>
            <div class="clear"></div>
        </div>
    </div>
    <div class="line"></div>
    <!-- Message -->

    <!-- Main content wrapper -->
    <div class="wrapper">

        <!-- Form -->
        <form style="width: 50%" class="form" id="form" action="" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <fieldset>
                <div class="widget">
                    <div class="formRow">
                        <label class="formLeft" for="param_name">Mã khách hàng:<span class="req">*</span></label>
                        <div class="formRight">
                            <span class="oneTwo"><input style='width:105px' name="id_customer" value="" _autocheck="true" class="format_number" type="text" required /></span>
                            <span name="id_customer_autocheck" class="autocheck"></span>
                            <div name="id_customer_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label class="formLeft" for="param_name">Mã sản phẩm:<span class="req">*</span></label>
                        <div class="formRight">
                            <span class="oneTwo"><input style='width:105px' name="id_product" value="" _autocheck="true" type="text" class="format_number" required /></span>
                            <span name="id_product_autocheck" class="autocheck"></span>
                            <div name="id_product_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <!-- Price -->
                    <div class="formRow">
                        <label class="formLeft" for="param_price">
                            Số lượng:<span class="req">*</span>
                        </label>
                        <div class="formRight">
                            <span class="oneTwo">
                                <input name="quantity" value="" style='width:105px' id="param_price" class="format_number" _autocheck="true" type="text" required/>
                            </span>
                            <span name="quantity_autocheck" class="autocheck"></span>
                            <div name="quantity_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label class="formLeft" for="param_price">
                            Hình thức thanh toán:<span class="req">*</span>
                        </label>
                        <div class="formRight">
                            <select name="payment" id='payment' class="left">
                                <option value="COD" selected>COD</option>
                                <option value="ATM">ATM</option>
                            </select>
                            <span name="payment_autocheck" class="autocheck"></span>
                            <div name="payment_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label class="formLeft" for="param_price">
                            Ghi chú:
                        </label>
                        <div class="formRight">
                            <textarea name="note" style='width:200px' _autocheck="true"></textarea>
                            <span name="note_autocheck" class="autocheck"></span>
                            <div name="note_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="formRow">
                        <label class="formLeft" for="param_cat">Trạng thái:</label>
                        <div class="formRight">
                            <select name="status" id='status' class="left">
                                <option value="0" selected>Chờ xử lý</option>
                                <option value="1">Đã tiếp nhận</option>
                                <option value="2">Đang giao hàng</option>
                                <option value="3">Đã nhận hàng</option>
                            </select>
                            <span name="cat_autocheck" ></span>
                            <div name="cat_error" class="clear error"></div>
                        </div>
                        <div class="clear"></div>
                    </div>

                    <div class="clear"></div>
                    <div style="align-items: center" class="formSubmit">
                        <input type="submit" id="create_order" value="Tạo đơn" class="redB" />
                    </div>
                    <div class="clear"></div>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="clear mt30"></div>
@endsection