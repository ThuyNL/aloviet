@extends('admin.layout')
@section('content')
<div class="titleArea">
    <div class="wrapper">
        <div class="pageTitle">
            <h5>Doanh thu</h5>
            <span>Doanh thu của các đơn hàng đã giao thành công</span>
        </div>

        <div class="horControlB menu_action">
            <ul>
                <li><a href="{{route('revenue')}}">
                        <img src="source/backend/admin/images/icons/control/16/list.png" />
                        <span>Danh sách</span>
                    </a></li>

                <li><a href="admin/tran/export">
                        <img src="source/backend/admin/images/excel.png" />
                        <span>Xuất file excel</span>
                    </a></li>

            </ul>
        </div>

        <div class="clear"></div>
    </div>
</div>
<div class="line"></div>

<!-- Main content wrapper -->
<div class="wrapper">

    <div class="widget">
        @if($revenue)
        <table cellpadding="0" cellspacing="0" width="100%" class="sTable mTable myTable" id="checkAll">

            <thead class="filter"><tr><td colspan="11">
                    <form class="list_filter form" action="{{route('search_revenue')}}" method="get">
                        <table cellpadding="0" cellspacing="0" width="100%"><tbody>

                            <tr>

                                <td class="label" style="width:60px;"><label for="filter_created">Từ ngày</label></td>
                                <td class="item"><input name="date_from" value="" id="filter_created" type="text" class="datepicker" /></td>

                                <td class="label"><label for="filter_created_to">Đến ngày</label></td>
                                <td class="item"><input name="date_to" value="" id="filter_created_to" type="text" class="datepicker" /></td>

                                <td colspan='2' style='width:60px'>
                                    <input type="submit" id="search" class="button blueB" value="Tìm kiếm" />
                                    <input type="reset" class="basic" value="Reset" onclick="window.location.href = 'admin/transaction/view'; ">
                                </td>
                            </tr>
                            </tbody></table>
                    </form>
                </td></tr></thead>

            <tbody class="list_item">
                <tr style="height: 100px; font-size: 16px;font-weight: bold; text-align: center">
                <td class="textC title" style="width:30%;">Doanh thu</td>

                <td>
                    {{number_format($revenue)}} VNĐ
                </td>
            </tr>
            </tbody>

        </table>
        @else
            <h5 style="margin: 15px">Không có dữ liệu</h5>
        @endif
    </div>

</div>
<div class="clear mt30"></div>
@endsection