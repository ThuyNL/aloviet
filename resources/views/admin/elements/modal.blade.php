<!-- modal -->
<div class="modal" id="modal-confirm" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span>
                </button>
                <h4 class="modal-title" id="modal-label">Xác nhận</h4>
            </div>
            <div class="modal-body">Đơn hàng sẽ bị xóa bỏ và hoàn lại sản phẩm？</div>
            <div class="modal-footer">
                <form class="form-confirm" method="post">
                    <input type="hidden" name="_method" value="refund"/>
                    {{ csrf_field() }}
                    <button type="button" class="btn btn-default" data-dismiss="modal">Hủy bỏ</button>
                    <button type="submit" class="btn btn-primary remove-record">Đồng ý</button>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- /modal -->
{{--    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>--}}
    <script type="text/javascript">
        $(document).ready(function(){
            $('#modal-confirm').on('shown.bs.modal', function(e){
                var urlRefund = $(e.relatedTarget).data('url');
                $(this).find('.form-confirm').attr('action', urlRefund);
            });
            // $('.datepicker').datepicker();
            $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
        })
    </script>
