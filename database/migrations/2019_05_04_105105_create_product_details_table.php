<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_details', function (Blueprint $table) {
            $table->increments('id');
            $table->text('screen')->nullable();
            $table->text('system')->nullable();
            $table->text('camera_after')->nullable();
            $table->text('camera_before')->nullable();
            $table->text('cpu')->nullable();
            $table->text('ram')->nullable();
            $table->text('cache_in')->nullable();
            $table->text('cache_tag')->nullable();
            $table->text('sim_tag')->nullable();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_details');
    }
}
